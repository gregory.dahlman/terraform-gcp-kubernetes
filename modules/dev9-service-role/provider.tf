provider "google" {
  credentials = "${file("~/.config/gcloud/mensor-tf-admin.json")}"
  region      = "us-west1"
  version     = "~> 1.8"
}
