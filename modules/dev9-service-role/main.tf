#################
###
### creating a Dev9 role specifically for the terraform service account
###
resource "google_service_account" "service_account" {
  account_id   = "service-account-tf"
  display_name = "Service Account Project TF"
  project      = "${var.project}"
}

### /* Grant permissions to service account */ ###
resource "google_organization_iam_policy" "policy" {
  count = "${length(var.roles)}"

  org_id      = "${var.org_id}"
  policy_data = "${data.google_iam_policy.admin.*.policy_data[count.index]}"
}

data "google_iam_policy" "admin" {
  count = "${length(var.roles)}"

  binding {
    role = "${element(var.roles, count.index)}"

    members = [
      # "serviceAccount:${var.service_account_email}",
      "serviceAccount:${google_service_account.service_account.email}",
    ]
  }
}

# resource "google_project_iam_custom_role" "dev9_service_account" {
#   role_id     = "${var.role_id}"
#   title       = "${var.role_name}"
#   description = "${var.description}"
#   permissions = "$(concat(var.permissions, var.additional_permissions)}"
# }

