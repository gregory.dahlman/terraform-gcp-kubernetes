# Overview

This will augment the newly created Terraform Service Account with proper roles & permissions to be able to:

* Create/Modify Projects
* Create/Modify folders

## Replaces

This script replaces the following:

    gcloud organizations add-iam-policy-binding 504602530426 --member serviceAccount:terraform@mike-terraform-project.iam.gserviceaccount.com --role roles/resourcemanager.projectCreator


```bash
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/billing.admin
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/orgpolicy.policyAdmin
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/resourcemanager.folderAdmin
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/resourcemanager.folderIamAdmin
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/resourcemanager.lienModifier
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/owner
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/resourcemanager.projectDeleter
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/resourcemanager.projectCreator
gcloud organizations add-iam-policy-binding 0123456789012 --member serviceAccount:terraform@some-user-name-here.iam.gserviceaccount.com --role roles/resourcemanager.projectMover
```