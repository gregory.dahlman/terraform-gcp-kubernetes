variable "project" {
  type        = "string"
  description = "Name of the project for DB to be constructed in"
}

variable "region" {
  type        = "string"
  description = "Region to run DB in"
  default     = "us-west1"
}

variable "database_name" {
  type        = "string"
  description = "Name of the database"
}

variable "dbversion" {
  type        = "string"
  description = "Version of DB"
  default     = "MYSQL_5_7"
}

variable "tier" {
  type    = "string"
  default = "db-f1-micro"
}

variable "disk_size" {
  default = 10
}

variable db_charset {
  description = "The charset for the default database"
  default     = "utf8"
}

variable db_collation {
  description = "The collation for the default database. Example for MySQL databases: 'utf8', and Postgres: 'en_US.UTF8'"
  default     = "utf8"
}

variable "activation_policy" {
  description = "This specifies when the instance should be active. Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`."
  default     = "ALWAYS"
}

variable "dbuser" {
  type        = "string"
  description = "User for DB"
}

variable "dbpassword" {
  type        = "string"
  description = "Initial password for DB User"
}

variable "dbhost" {
  description = "The host for the default user"
  default     = "%"
}
