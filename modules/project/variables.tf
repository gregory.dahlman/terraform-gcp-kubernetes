variable "project_name" {
  type        = "string"
  description = "Name of the intended project"
}

variable "folder_name" {
  type        = "string"
  description = "Name of the folder"
  default     = "administration"
}

variable "billing_account" {
  type        = "string"
  description = "Billing account to associate with project"
}

variable "org_id" {
  type        = "string"
  description = "Organization ID parent of the project"
}

variable "owner_email" {
  type        = "string"
  description = "User's email address for owner (not Service Account)"
}

variable "additional_apis" {
  type        = "list"
  default     = []
  description = "List of Additional APIs to enable for project"
}

variable "roles" {
  type        = "list"
  default     = []
  description = "List of roles applied to folder for a USER"
}
