variable "project" {
  type        = "string"
  default     = "mike-terraform-project"
  description = "Project to create Service Account in"
}

variable "serviceAccount" {
  type = "string"

  # TODO: REMOVE THIS
  default     = "terraform@mike-terraform-project.iam.gserviceaccount.com"
  description = "Full email address of service account to create project-specific service account"
}
